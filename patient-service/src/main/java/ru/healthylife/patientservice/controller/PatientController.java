package ru.healthylife.patientservice.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/patients")
public class PatientController {

    @GetMapping("/me")
    public String resource(@AuthenticationPrincipal Jwt jwt) {
        return String.format("Resource with subjectId: %s)" ,
                jwt.getSubject());
    }
}
