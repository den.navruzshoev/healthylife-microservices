package ru.healthylife.authservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "spring.security.provider.healthylife")
public class ProviderSettingsConfig {
    private static final String DEFAULT_AUTHORIZATION_ENDPOINT = "/oauth2/authorize";
    private static final String DEFAULT_TOKEN_ENDPOINT = "/oauth2/token";
    private static final String DEFAULT_JWK_SET_ENDPOINT = "/oauth2/jwks";
    private static final String DEFAULT_TOKEN_REVOCATION_ENDPOINT = "/oauth2/revoke";
    private static final String DEFAULT_TOKEN_INTROSPECTION_ENDPOINT = "/oauth2/introspect";
    private static final String DEFAULT_OIDC_CLIENT_REGISTRATION_ENDPOINT = "/connect/register";

    private String issuerEndpoint;
    private String authorizationEndpoint = DEFAULT_AUTHORIZATION_ENDPOINT;
    private String tokenEndpoint = DEFAULT_TOKEN_ENDPOINT;
    private String jwkSetEndpoint = DEFAULT_JWK_SET_ENDPOINT;
    private String tokenRevocationEndpoint = DEFAULT_TOKEN_REVOCATION_ENDPOINT;
    private String tokenIntrospectionEndpoint = DEFAULT_TOKEN_INTROSPECTION_ENDPOINT;
    private String oidcClientRegistrationEndpoint = DEFAULT_OIDC_CLIENT_REGISTRATION_ENDPOINT;
}
