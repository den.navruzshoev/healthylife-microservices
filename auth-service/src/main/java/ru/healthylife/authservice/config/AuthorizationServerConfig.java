package ru.healthylife.authservice.config;

import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import lombok.NonNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.OAuth2AuthorizationServerConfiguration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.server.authorization.client.InMemoryRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.config.ProviderSettings;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import ru.healthylife.authservice.util.JwksUtils;

import java.util.UUID;


@Configuration(proxyBeanMethods = false)
public class AuthorizationServerConfig {

    @Bean
    @NonNull
    @Order(Ordered.HIGHEST_PRECEDENCE)
    SecurityFilterChain authorizationServerSecurityFilterChain(@NonNull HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/auth2/**").authenticated();
        OAuth2AuthorizationServerConfiguration.applyDefaultSecurity(http);
        http.formLogin(Customizer.withDefaults());
        return http.build();
    }

    @Bean
    @NonNull
    public JWKSource<SecurityContext> jwkSource() {
        RSAKey rsaKey = JwksUtils.generateRsa();
        JWKSet jwkSet = new JWKSet(rsaKey);
        return (jwkSelector, securityContext) -> jwkSelector.select(jwkSet);
    }

    @Bean
    @NonNull
    public RegisteredClientRepository registeredClientRepository() {
        RegisteredClient registeredClient = RegisteredClient.withId(UUID.randomUUID().toString())
                .clientId("patient-service")
                .clientSecret("{noop}secret")
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_POST)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .redirectUri("http://127.0.0.1:8080/login/oauth2/code/gateway")
                .scope(OidcScopes.OPENID)
                .scope("message.read")
                .scope("patient.write")
                .build();
        return new InMemoryRegisteredClientRepository(registeredClient);
    }

    @Bean
    @NonNull
    public UserDetailsService users() {
        UserDetails user = User.withDefaultPasswordEncoder()
                .username("user")
                .password("password")
                .roles("USER")
                .build();
        return new InMemoryUserDetailsManager(user);
    }

    @Bean
    @NonNull
    public ProviderSettings providerSettings(@NonNull ProviderSettingsConfig providerSettingsConfig) {
        ProviderSettings.Builder providerSettingsBuilder = ProviderSettings.builder();

        providerSettingsBuilder
                .authorizationEndpoint(providerSettingsConfig.getAuthorizationEndpoint())
                .jwkSetEndpoint(providerSettingsConfig.getJwkSetEndpoint())
                .oidcClientRegistrationEndpoint(providerSettingsConfig.getOidcClientRegistrationEndpoint())
                .tokenEndpoint(providerSettingsConfig.getTokenEndpoint())
                .tokenIntrospectionEndpoint(providerSettingsConfig.getTokenIntrospectionEndpoint())
                .tokenRevocationEndpoint(providerSettingsConfig.getTokenRevocationEndpoint());

        if (!providerSettingsConfig.getIssuerEndpoint().isBlank()) {
            providerSettingsBuilder.issuer(providerSettingsConfig.getIssuerEndpoint());
        }
        return providerSettingsBuilder.build();
    }
}
