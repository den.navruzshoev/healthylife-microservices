package ru.healthylife.authservice.util;


import com.nimbusds.jose.jwk.RSAKey;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Unmodifiable;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.UUID;

@UtilityClass
public class JwksUtils {

    @NonNull
    @Unmodifiable
    public static RSAKey generateRsa() {
        KeyPair keyPair = KeyPairGeneratorUtils.generateRsaKeyPair();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

        return new RSAKey.Builder(publicKey)
                .privateKey(privateKey)
                .keyID(UUID.randomUUID().toString())
                .build();
    }
}
